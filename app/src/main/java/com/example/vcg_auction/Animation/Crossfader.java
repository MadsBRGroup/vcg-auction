package com.example.vcg_auction.Animation;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.widget.TextView;

public class Crossfader {
        TextView to_appear,to_remove;
        Long dur;
        public Crossfader(TextView v1, TextView v2,Long dur){
        this.to_remove = v1;
        this.to_appear = v2;
        this.dur = dur;
        }


    public void animate(){

        to_appear.animate()
                .alpha(1f)
                .setDuration(dur)
                .setListener(
                        new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                             }
                        } );



        to_remove.animate()
                .alpha(0f)
                .scaleX(1f)
                .scaleY(1f)
                .setDuration(dur)
                .setListener(
                        new AnimatorListenerAdapter() {
                            @Override
                            public void onAnimationEnd(Animator animation) {
                                to_remove.setVisibility(TextView.INVISIBLE);
                            }
                        });
    }
}

