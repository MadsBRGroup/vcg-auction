package com.example.vcg_auction.Animation;

import android.view.View;

public class MyRunnable implements Runnable {
    private View v;
    private Long dur;
    public MyRunnable(Long dur, View v) {
        this.v = v;
        this.dur = dur;
    }
    @Override
    public void run() {
    v.animate()
            .alpha(1f)
            .setDuration(dur)
            .setListener(null);
    }

}
