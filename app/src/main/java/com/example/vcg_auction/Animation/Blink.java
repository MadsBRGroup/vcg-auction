package com.example.vcg_auction.Animation;

import android.widget.TextView;

public class Blink {

    TextView text,bold_text;
    Long dur;
    public Blink(TextView v1, TextView v2,Long dur){
        this.bold_text = v1;
        this.text = v2;
        this.dur = dur;
    }

    public void animate_blink(String str){
    bold_text.setText(str);
    bold_text.setAlpha(1f);
    bold_text.setScaleX(1.5f);
    bold_text.setScaleY(1.5f);

        bold_text.setVisibility(TextView.VISIBLE);
    text.setAlpha(0f);

        text.setText(str);
    Crossfader anim = new Crossfader(bold_text,text,dur);
    anim.animate();


    }

}
