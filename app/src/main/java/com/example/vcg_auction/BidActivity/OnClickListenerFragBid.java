package com.example.vcg_auction.BidActivity;

import android.view.View;

import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.vcg_auction.R;

public class OnClickListenerFragBid implements View.OnClickListener {
    public Fragment_Choose_Good frag;
    FragmentManager manager;
    int btn_nr; // color nr amount
    FragmentTransaction transaction;
    BidActivity activity;


    public OnClickListenerFragBid(BidActivity activity,Fragment_Choose_Good frag,int btn_nr){
        super();
        this.activity = activity;
        this.frag = frag;
        this.btn_nr = btn_nr; //there should be color_nr
    }

    @Override
    public void onClick(View v){
        activity.set_btn_properties(btn_nr,frag.btn_expanded);
        activity.close_or_swap_frag(frag.btn_expanded);
    }
}
