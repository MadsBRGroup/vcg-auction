package com.example.vcg_auction.BidActivity;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.example.vcg_auction.Animation.Blink;

public class Bid_Nr_Update implements TextWatcher {
    TextView v;
    String str;
    Blink blink;
    public static int REMAINING;
    int c;
    int color_nr = 7; // fix
    Boolean empty_before_change = true;
    Boolean empty_after_change = false;

    public Bid_Nr_Update(TextView v,TextView bold){
        super();
        this.blink = new Blink(bold,v,250L);
        this.v = v;
        str = v.getText().toString();
        REMAINING = Character.getNumericValue(str.charAt(str.length()-1)); // Equals player_nr
    }

    @Override
    public void beforeTextChanged(CharSequence ch, int s , int e, int c)
    {
        empty_before_change =ch.toString().trim().isEmpty();
    }

    @Override
    public void onTextChanged(CharSequence ch, int s , int e, int c)
    {
        empty_after_change =ch.toString().trim().isEmpty();
    }


    @Override
    public void afterTextChanged(Editable s) {
     if(empty_after_change != empty_before_change){
         if(empty_after_change){
            REMAINING += 1;
            str = String.valueOf(REMAINING);
        }

        else{
             REMAINING -= 1;
             c = Math.max(0,REMAINING);
             str =String.valueOf(c);
        }
        blink.animate_blink(str);
     }
    }
}
