package com.example.vcg_auction.BidActivity;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.core.widget.TextViewCompat;
import androidx.fragment.app.Fragment;

import com.example.vcg_auction.Auxillary.Bid_Info;
import com.example.vcg_auction.R;
import com.example.vcg_auction.Auxillary.ArrayMathStuff;
import com.example.vcg_auction.VCG.VCG_Holder;

public class Fragment_Choose_Good extends Fragment {
    public Bid_Info info;
    public Button[] btns;
    public boolean[] are_colors_bidded;
    public int [] bidded_goods;
    public String[] names;
    public String[] colors;
    public int summoner_index;
    BidActivity activity;

    int btn_expanded = -1;

    VCG_Holder vcg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        activity = ((BidActivity) getActivity());
        info = (Bid_Info) activity.get_Info();
        colors = info.profile.colors;
        names = info.profile.names;
        container.setBackgroundColor(Color.parseColor("#D3D3D3"));
        return (ViewGroup) inflater.inflate(
                R.layout.fragment_choose_good, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        int color_nr = info.color_nr;
        int player_nr = info.player_nr;
        btns = new Button[color_nr];
        LinearLayout lin   = view.findViewById(R.id.linbtn);
        LinearLayout lintwo   = view.findViewById(R.id.linbtn2);

        bidded_goods = activity.bidded_goods_and_none;

        int height = (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 46, getResources().getDisplayMetrics());;
        for (int i = 0; i < color_nr; i++) {
            btns[i] = new Button(this.getContext());
            btns[i].setPadding(3,3,3,3);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,height);
            param.setMargins(10,10,10,10);
            btns[i].setLayoutParams(param);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(btns[i], TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            btns[i].setBackground(activity.button_drawer(Color.parseColor(colors[i])));
            btns[i].setText(names[i]);
            btns[i].setOnClickListener(new OnClickListenerFragBid(activity,this,i));
            if(i % 2 == 0){
                lin.addView(btns[i]);
            }
            else{
                lintwo.addView(btns[i]);
            }
            if(ArrayMathStuff.contains(bidded_goods,i)){
                btns[i].setClickable(false);
                btns[i].setAlpha(0.1f);
            }
        }
    }


    public void minimize(){

    }

}