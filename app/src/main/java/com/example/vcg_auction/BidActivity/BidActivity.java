package com.example.vcg_auction.BidActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.TextViewCompat;
import androidx.fragment.app.FragmentTransaction;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.example.vcg_auction.Auxillary.Bid_Info;
import com.example.vcg_auction.R;
import com.example.vcg_auction.ResultActivity.Result;
import com.example.vcg_auction.Auxillary.ArrayMathStuff;
import java.io.Serializable;


public class BidActivity extends AppCompatActivity {
    int player_nr;
    String[] initials;
    int color_nr;
    int col_play_min;
    String initial_string;
    private boolean TEST;
    Bid_Info info;
    Bid_Nr_Update[] watchers;
    EditText[] bid_views;
    EditText initials_view;
    TextView player_view, bid_remaining, bid_remaining_bold;
    Button confirm_view;
    Button[] btns;
    int[] btn_positions; // 0--player_nr chosen
    int persons_confirmed = -1;
    int[][] bids;
    int[] bidded_goods_and_none;
    int frame_id;
    int unfilled_nr;
    Fragment_Choose_Good frag = new Fragment_Choose_Good();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bid);
        info = (Bid_Info) getIntent().getSerializableExtra("options_info");
        TEST=info.TEST;
        final String[] colors = info.profile.colors;
        color_nr = info.color_nr;
        player_nr = info.player_nr;
        col_play_min = Math.min(color_nr, player_nr);
        btns = new Button[player_nr];
        bid_views = new EditText[col_play_min];
        watchers = new Bid_Nr_Update[player_nr];
        btn_positions = new int[player_nr];
        bidded_goods_and_none = new int[player_nr];
        final String[] names = info.profile.names;
        int[] bid_ids = new int[5];
        int[] btn_ids = new int[5];
        frame_id = R.id.good_change_layout1;

        //Setup buttons
        bid_ids[0] = R.id.bidEntry1;
        bid_ids[1] = R.id.bidEntry2;
        bid_ids[2] = R.id.bidEntry3;
        bid_ids[3] = R.id.bidEntry4;
        bid_ids[4] = R.id.bidEntry5;
        btn_ids[0] = R.id.btn1;
        btn_ids[1] = R.id.btn2;
        btn_ids[2] = R.id.btn3;
        btn_ids[3] = R.id.btn4;
        btn_ids[4] = R.id.btn5;
        for (int i = 0; i < col_play_min; i++) {
            btns[i] = findViewById(btn_ids[i]);
            bid_views[i] = (EditText) findViewById(bid_ids[i]);
            TextViewCompat.setAutoSizeTextTypeWithDefaults(btns[i], TextViewCompat.AUTO_SIZE_TEXT_TYPE_UNIFORM);
            btns[i].setPadding(3, 3, 3, 3);
            btns[i].setOnClickListener(new OnClickListenerWithArg(this, frag, i));
        }
        for (int i = col_play_min; i < 5; i++) {
            findViewById(bid_ids[i]).setVisibility(View.INVISIBLE);
            findViewById(btn_ids[i]).setVisibility(View.INVISIBLE);
        }

        //Headline setup

        bid_remaining = findViewById(R.id.bid_remaining);
        bid_remaining_bold = findViewById(R.id.bid_remaining_bold);
        bid_remaining.setText(String.valueOf(player_nr));
        bid_remaining_bold.setText(String.valueOf(player_nr));
        CharSequence x = "";
        initials = new String[player_nr];
        bids = new int[player_nr][color_nr];
        EditText e;
        for (int i = 0; i < col_play_min; i++) {
            watchers[i] = new Bid_Nr_Update(bid_remaining, bid_remaining_bold);
            bid_views[i].addTextChangedListener(watchers[i]);
        }
        initials_view = (EditText) findViewById(R.id.initials);
        initials_view.setHint(x);
        player_view = findViewById(R.id.player);
        confirm_view = (Button) findViewById(R.id.confirm);


        //Initialize
        cleanup();
        confirm_view.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        for(int j = 0 ; j<col_play_min;j++){
                            if(bidded_goods_and_none[j] == color_nr){
                                unfilled_nr +=1;
                            }
                        }
                        if(unfilled_nr>0){
                            new AlertDialog.Builder(view.getContext())
                                    .setMessage("You did not place all of your bids. Do you want to continue anyway?")
                                    .setCancelable(false)
                                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            handle_confirm_press();
                                        }
                                    })
                                    .setNegativeButton("No", null)
                                    .show();

                        } else{
                            handle_confirm_press();
                        }
                    }
                }
        );
        initials_view.requestFocus();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setMessage("Do you want to cancel this auction?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                    }
                })
                .setNegativeButton("No", null)
                .show();
    }


    public void handle_confirm_press(){
        int temp_int = 0;
        String bid_string;
        initial_string = initials_view.getText().toString().trim();
        if (initial_string.isEmpty()) {
            initials_view.setError("Must not be empty");
            initials_view.requestFocus();
            return;
        }
        if (ArrayMathStuff.contains(initials, initial_string)) {
            initials_view.setError("Initials already taken");
            initials_view.requestFocus();
            return;
        }
        initials[persons_confirmed] = initial_string;

        for (int i = 0; i < col_play_min; i++) {
            int cut = 0;
            boolean did_cut = false;
            bid_string = bid_views[i].getText().toString();
            if (bid_string.indexOf(",") != -1) {
                cut = bid_string.indexOf(",");
                did_cut = true;
            }
            if (bid_string.indexOf(".") != -1) {
                cut = bid_string.indexOf(".");
                did_cut = true;
            }
            if (did_cut) {
                bid_string = bid_string.substring(0, cut);
            }
            if (bid_string.trim().isEmpty()) {
                temp_int = 0;
            } else {
                temp_int = Integer.parseInt(bid_string);
            }
            int[] items_not_chosen = new int[color_nr-(col_play_min-unfilled_nr)];
            int index = 0;
            for(int j = 0;j<color_nr;j++){
                if(!ArrayMathStuff.contains(bidded_goods_and_none,j)){
                    items_not_chosen[index]=j;
                    index +=1;
                }
            }
            if(bidded_goods_and_none[i]<color_nr){
                bids[persons_confirmed][bidded_goods_and_none[i]] = Math.max(temp_int, 0);
            }
        }

        if ((persons_confirmed < player_nr - 1)) {
            cleanup();
        } else {
            Bid_Info bidtoNextPage = new Bid_Info(initials, bids, info.secret_bids, info.profile, player_nr, color_nr,TEST);
            Intent intent = new Intent(this, Result.class);
            intent.putExtra("info", (Serializable) bidtoNextPage);
            startActivity(intent);
            finish();
        }
    }

    public void cleanup() {
        for (int i = 0; i < col_play_min; i++) {
            CharSequence emp = "";
            unfilled_nr = 0;
            bid_views[i].setText(emp);
            CharSequence ch = "bid";
            if(TEST){
                set_btn_properties(i, i);
            }
            else{
                set_btn_properties(color_nr, i);
            }
            bid_views[i].setHint(ch);
            }

        CharSequence emp = "";
        initials_view.setText(emp);
        CharSequence x = "";
        initials_view.setHint(x);
        bid_remaining.setText(String.valueOf(player_nr));
        bid_remaining_bold.setText(String.valueOf(player_nr));
        persons_confirmed += 1;
        String str = "Player " + Integer.toString(persons_confirmed + 1);
        player_view.setText(str);
        initials_view.requestFocus();
        if (TEST) {
            int test_bid;
            initials_view.setText("PL" + Integer.toString(persons_confirmed));
            for (int k = 0; k < Math.max(col_play_min-1,1); k++) {
                {
                    test_bid = 2 * (persons_confirmed +2)*(persons_confirmed +2)+ k;
                    bid_views[k].setText(Integer.toString(test_bid));
                }
            }
        }
    }


    public Bid_Info get_Info() {
        return info;
    }

    public GradientDrawable button_drawer(int color) {
        GradientDrawable drawable = new GradientDrawable();
        drawable.setShape(GradientDrawable.RECTANGLE);
        drawable.setColor(color);
        drawable.setCornerRadius(15f);
        drawable.setStroke(5, Color.BLACK);
        return drawable;
    }


    public void set_btn_properties(int good, int btn_nr) {
        int col;
        if (good < color_nr) {
             col = Color.parseColor(info.profile.colors[good]);
            btns[btn_nr].setText(info.profile.names[good]);
        } else{
            col = Color.LTGRAY;
            btns[btn_nr].setText("Choose Item");
        }
        btns[btn_nr].setBackground(button_drawer(col));
        btns[btn_nr].setGravity(Gravity.CENTER);
        bidded_goods_and_none[btn_nr] = good;

    }


    public void open_frag(int btn_nr) {
        for(View v : bid_views) {
            v.setVisibility(View.INVISIBLE);
        }
        confirm_view.setClickable(false);
        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
        transaction.add(frame_id, frag);
        transaction.commitNow();
        frag.btn_expanded = btn_nr;
    }


    public void close_or_swap_frag(int btn_nr) {
        int closed = close_frag();
        if (btn_nr != closed) {
            open_frag(btn_nr);
        } else {
            for (View v : bid_views) {
                v.setVisibility(View.VISIBLE);
            }
            confirm_view.setClickable(true);
        }
    }

    //helper to closer_or_swap_frag
    private int close_frag() {
        FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
        transaction.remove(frag);
        transaction.commitNow();
        int closed_position = frag.btn_expanded;
        frag.btn_expanded = -1;
        return closed_position;
    }
}



