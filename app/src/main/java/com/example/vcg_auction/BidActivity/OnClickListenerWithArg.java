package com.example.vcg_auction.BidActivity;

import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.vcg_auction.R;

public class OnClickListenerWithArg implements View.OnClickListener {
    public Fragment_Choose_Good frag;
    FragmentManager manager;
    int btn_nr;
    FragmentTransaction transaction;
    BidActivity activity;
    int[] bidded_goods;
    public OnClickListenerWithArg(BidActivity activity, Fragment_Choose_Good frag, int btn_nr){
        super();
        this.activity = activity;
        this.frag = frag;
        this.btn_nr = btn_nr;

    }

    @Override
    public void onClick(View v){
         activity.close_or_swap_frag(btn_nr);
    }
}
