package com.example.vcg_auction.ResultActivity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.vcg_auction.Auxillary.Bid_Info;
import com.example.vcg_auction.R;
import com.example.vcg_auction.VCG.VCG_Holder;

public class Fragment_BidsTable extends Fragment {
    public TextView[][] entries;
    public Bid_Info info;
    int color_nr;
    int player_nr;
    VCG_Holder vcg;
    LinearLayout[] lin_lay;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        info = (Bid_Info) getActivity().getIntent().getSerializableExtra("info");
        vcg = ((Result) getActivity()).get_vcgHolder();
        color_nr= info.bids[0].length;
        player_nr = info.bids.length;
        entries = new TextView[color_nr +1 ][player_nr +1 ];
        lin_lay = new LinearLayout[6]; //TODO: ADD MORE PLAYER FUNCTIONALITY
        return (ViewGroup) inflater.inflate(
                R.layout.fragment_bidstable, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        int[] rowIDs = {R.id.col_start,R.id.col0,R.id.col1,R.id.col2,R.id.col3,R.id.col4};
        float WEIGHT_ROWS = 1f/( player_nr + 1);
        float WEIGHT_COLS = 1f/( color_nr + 1);
        for(int i = 0; i<6;i++){
            lin_lay[i] = view.findViewById(rowIDs[i]);
            if(i<player_nr + 1) {
                lin_lay[i].setOrientation(LinearLayout.VERTICAL);
                lin_lay[i].setGravity(Gravity.CENTER);
                if (i % 2 == 1) {
                    lin_lay[i].setBackgroundColor(Color.LTGRAY);
                }
                lin_lay[i].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, WEIGHT_ROWS));
            }
            else {
                lin_lay[i].setVisibility(View.GONE);
            }
        }

        String str;
        entries[0][0] = new TextView(getContext());
        entries[0][0].setGravity(Gravity.CENTER);
        entries[0][0].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,WEIGHT_COLS));
        lin_lay[0].addView(entries[0][0]);
        int split_index;
        String[] splitnames;
        for(int i = 0;i<color_nr + 1;i++){
            for (int j = 0;j<player_nr + 1;j++){
                if(i > 0 | j > 0){
                    entries[i][j] = new TextView(getContext());
                    entries[i][j].setTextSize(20);
                    entries[i][j].setGravity(Gravity.CENTER);
                    entries[i][j].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT,WEIGHT_COLS));
                    if( i == 0) {
                        str = info.initials[j-1];
                        entries[i][j].setTypeface(null, Typeface.BOLD);
                        entries[i][j].setTextColor(Color.BLACK);
                    } else if (j == 0){
                        str = "";
                        split_index= info.profile.names[i-1].indexOf("\n");
                        if(split_index>-1){
                            splitnames = new String[2];
                            splitnames[0] = info.profile.names[i-1].substring(0,split_index);
                            splitnames[1] = info.profile.names[i-1].substring(split_index+1);
                        }
                        else{
                            splitnames = new String[1];
                            splitnames[0] = info.profile.names[i-1];
                        }
                        for(int k = 0;k<splitnames.length;k++){
                            if(splitnames[k].length()>9)
                            splitnames[k] = splitnames[k].substring(0,7) + "...";
                        }
                        if(split_index>-1){
                            str = splitnames[0] + "\n" + splitnames[1];
                        }
                        else{
                            str =splitnames[0];
                        }
                        entries[i][j].setTextColor(Color.parseColor(info.profile.colors[i-1]));
                        entries[i][j].setAutoSizeTextTypeWithDefaults(TextView.AUTO_SIZE_TEXT_TYPE_UNIFORM);
                        entries[i][j].setMaxLines(2);

                    } else {
                        str = "" + info.bids[j - 1][i - 1];
                    }
                    entries[i][j].setText(str);
                    lin_lay[j].addView(entries[i][j]);
                }
             }
        }
        int circle_x;
        int circle_y;
        for(int i = 0 ; i<Math.min(color_nr,player_nr);i++){
            circle_x = vcg.dist[i]+1;
            circle_y = vcg.winners[i]+1;
            entries[circle_x][circle_y].setTypeface(null, Typeface.BOLD);
            entries[circle_x][circle_y].setBackgroundResource(R.drawable.circleboundary);
        }
    }
}



