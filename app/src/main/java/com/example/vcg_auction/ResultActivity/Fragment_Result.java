package com.example.vcg_auction.ResultActivity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.example.vcg_auction.Animation.MyRunnable;
import com.example.vcg_auction.Auxillary.Bid_Info;
import com.example.vcg_auction.R;
import com.example.vcg_auction.VCG.VCG_Holder;

//https://stackoverflow.com/questions/54643379/proper-implementation-of-viewpager2-in-android
public class Fragment_Result extends Fragment {
    public Bid_Info info;
    public TextView[] result_rows;
    VCG_Holder vcg;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        info = (Bid_Info) getActivity().getIntent().getSerializableExtra("info");
        vcg = ((Result) getActivity()).get_vcgHolder();
        return (ViewGroup) inflater.inflate(
                R.layout.fragment_result, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        int color_nr = info.color_nr;
        int player_nr = info.player_nr;
        int col_play_min = Math.min(color_nr,player_nr);
        int[] colors = new int[color_nr];
        for (int i = 0; i < color_nr; i++) {
            colors[i] = Color.parseColor(info.profile.colors[i]);
        }
        String[] names = new String[color_nr];
        for(int i = 0; i < names.length;i++){
            names[i] = info.profile.names[i];
            names[i] = names[i].replaceAll("[\\n]", "/");
        }
        int[] initial_lengths = new int[player_nr];
        result_rows = new TextView[player_nr];
        int[] resultViewIds = new int[5];
        resultViewIds[0] = R.id.resultp1;
        resultViewIds[1] = R.id.resultp2;
        resultViewIds[2] = R.id.resultp3;
        resultViewIds[3] = R.id.resultp4;
        resultViewIds[4] = R.id.resultp5;

        for (int i = 0; i < player_nr; i++) {
            result_rows[i] = (TextView) view.findViewById(resultViewIds[i]);
            result_rows[i].setVisibility(View.INVISIBLE);
        }
        for (int i = player_nr; i < 5; i++) {
            view.findViewById(resultViewIds[i]).setVisibility(View.GONE);
        }
        if (info.secret_bids)
        {
            view.findViewById(R.id.swipeText).setVisibility(View.GONE);
        }
        //create spannable strings
        SpannableString[] span_strings = new SpannableString[player_nr];
        for (int i = 0; i < col_play_min; i++) {
            StringBuffer strbuf = new StringBuffer();
            strbuf = strbuf.append(info.initials[vcg.winners[i]]).append(" gets ").append(names[vcg.dist[i]]);
            if(i <vcg.winners_found_nr){
                strbuf.append(" for ").append((int) vcg.price[i]).append(".");
                if (!info.secret_bids) {
                    strbuf.append(" bid : ").append(info.bids[vcg.winners[i]][vcg.dist[i]]);
                }
            }
            else
            {
                strbuf.append(" at random").append(".");
            }
            String str = strbuf.toString();
            span_strings[i] = new SpannableString(str);
            ForegroundColorSpan foregroundSpanOne = new ForegroundColorSpan(colors[vcg.dist[i]]);
            span_strings[i].setSpan(foregroundSpanOne, 0, info.initials[vcg.winners[i]].length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            ForegroundColorSpan foregroundSpanTwo = new ForegroundColorSpan(colors[vcg.dist[i]]);
            span_strings[i].setSpan(foregroundSpanTwo, info.initials[vcg.winners[i]].length() + 6, info.initials[vcg.winners[i]].length() + 6 + names[vcg.dist[i]].length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);



        }

        Long dur = 1500L;
        Handler[] handlers = new Handler[player_nr];
        for (int i = 0; i < player_nr; i++) {
            result_rows[i].setText(span_strings[i]);
            result_rows[i].setAlpha(0f);
            result_rows[i].setVisibility(View.VISIBLE);
            handlers[i] = new Handler();
            MyRunnable runzi = new MyRunnable(dur, result_rows[i]);
            handlers[i].postDelayed(runzi, 200L + (dur / 3L) * i);
        }
    }
/*

    @Override
    public void onResume(){
        super.onResume();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_USER_PORTRAIT);
    }
*/



}