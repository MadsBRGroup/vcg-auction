package com.example.vcg_auction.ResultActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import androidx.viewpager2.widget.ViewPager2;

import com.example.vcg_auction.Auxillary.Bid_Info;
import com.example.vcg_auction.R;
import com.example.vcg_auction.VCG.VCG_Auction;
import com.example.vcg_auction.VCG.VCG_Holder;
//https://stackoverflow.com/questions/54643379/proper-implementation-of-viewpager2-in-android

public class Result extends AppCompatActivity {
    TextView[] result_rows; 
    ViewPager2 myViewPager2;
    Result_Adapter myAdapter;
    public boolean TEST = false;
    public VCG_Auction vcg;
    Bid_Info info;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        info = (Bid_Info) getIntent().getSerializableExtra("info");
        TEST = info.TEST;
        vcg = new VCG_Auction(info.bids,TEST);
        vcg.run_VCG();
        setContentView(R.layout.activity_result);
        myViewPager2 = findViewById(R.id.pager);
        myViewPager2.setOffscreenPageLimit(1);
        final Bid_Info info = (Bid_Info) getIntent().getSerializableExtra("info");
        myAdapter = new Result_Adapter(getSupportFragmentManager(),getLifecycle(),info);
        if(!info.secret_bids){

            myAdapter.include_bidstable();
        }
        myViewPager2.setAdapter(myAdapter);
    }

    public VCG_Holder get_vcgHolder(){
        VCG_Holder v = vcg.constructHolder(info.bids);
        v.score = vcg.score;
        v.winners_found_nr = vcg.winners_found_nr;
        v.scores_wo_winner = vcg.scores_wo_winner;
        v.dist = vcg.dist;
        v.price = vcg.price;
        v.winners =vcg.winners;
        return v;
    }

}

