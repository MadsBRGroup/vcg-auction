package com.example.vcg_auction.ResultActivity;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.example.vcg_auction.Auxillary.Bid_Info;

public class Result_Adapter extends FragmentStateAdapter {
    int pages = 1;
    public Result_Adapter(@NonNull FragmentManager fragmentManager, @NonNull Lifecycle lifecycle, Bid_Info info) {
        super(fragmentManager, lifecycle);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        switch (position) {
            case 0:
                return new Fragment_Result();
            case 1:
                return new Fragment_BidsTable();

        }
        return null;

    }

    public void include_bidstable()
    {
        pages = 2;
    }

    @Override
    public int getItemCount() {
        return pages;
    }
}