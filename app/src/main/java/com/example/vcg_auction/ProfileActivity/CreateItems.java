package com.example.vcg_auction.ProfileActivity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import com.example.vcg_auction.Auxillary.MyDrawableCompat;
import com.example.vcg_auction.R;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import static com.example.vcg_auction.MainActivity.MainActivity.SAVED_PROFILES_NAME;

public class CreateItems extends AppCompatActivity {

    int currentcolor_id;
    Auction_Profile profile;
    Auction_Profile_Container container;
    int MAX_COLORS;
    int MAX_PLAYERS;
    String color_hex;
    ArrayList<Integer> btn_ids;
    ArrayList<Integer> btncol_ids;
    ArrayList<Integer> textView_ids;
    public Button[] btns;
    public Button create;
    public EditText[] texts;
    String[] colors = {"#FFA500", "#D3212D", "#0048BA", "#BEBEBE", "#676767", "#644117", "#FFE135", "#008000","#9370DB"};
    String[] color_names = {"orange"  ,  "red"    , "blue"    , "white", "grey"   , "brown"   , "yellow", "green", "purple"};
    String[] names = {"orange"  ,  "red"    , "blue"    , "white", "grey"   , "brown"   , "yellow", "green", "purple"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_item);
        LinearLayout rel = findViewById(R.id.profileRel );
        profile = (Auction_Profile) getIntent().getSerializableExtra("profile");
        MAX_COLORS = profile.MAX_COLOR_NR;
        MAX_PLAYERS= profile.MAX_PLAYER_NR;
        int height = 25;
        int width = 25;
        int current_id;
        btn_ids = new ArrayList<Integer>(MAX_COLORS);
        btncol_ids = new ArrayList<Integer>(MAX_COLORS);
        textView_ids = new ArrayList<Integer>(MAX_COLORS);
        btns = new Button[MAX_COLORS];
        texts = new EditText[MAX_COLORS];
        int[] textView_headline_ids = new int[3];
        LinearLayout hori = new LinearLayout(this);

        String[] headline_strings = { " Toggle ", " Color " , " Item Name" };
        for(int i = 0 ; i < 3;i++){
            TextView headline = new TextView(this);
            current_id = View.generateViewId();
            headline.setId(current_id);
            LinearLayout.LayoutParams paramText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            headline.setText(headline_strings[i]);
            hori.addView(headline, paramText);
        }
        LinearLayout.LayoutParams paramLin = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        rel.addView(hori,paramLin);


        Drawable drawable = getDrawable(android.R.drawable.ic_menu_close_clear_cancel);
        for(int i = 0 ; i<MAX_COLORS; i++){

            hori = new LinearLayout(this);
            paramLin = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            Button btn = new Button(this);
            current_id = View.generateViewId();
            btn.setId(current_id);
            btn_ids.add(i,current_id);
            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            MyDrawableCompat.setColorFilter(drawable, Color.RED);
            btn.setBackground(drawable);
            hori.addView(btn, param);


            LinearLayout.LayoutParams parambtn = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            Button btncol = new Button(this);
            currentcolor_id = View.generateViewId();
            btncol.setId(currentcolor_id);
            btns[i] = btncol;
            OnClickListenerChangeColor listener = new OnClickListenerChangeColor(this,i);
            btncol.setOnClickListener(listener);
            btncol_ids.add(i,currentcolor_id);
            hori.addView(btncol,parambtn);


            EditText name = new EditText(this);
            texts[i] = name;
            LinearLayout.LayoutParams paramText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            name.setHint("Name");
            hori.addView(name, paramText);
            View[] vs = {btncol,name};
            btn.setOnClickListener(new OnClickListenerToggleItem( vs ));
            rel.addView(hori,paramLin);
        }

        LinearLayout.LayoutParams param_new_item = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        Button btn_new_item = new Button( this);
        btn_new_item.setText("Add new item");

        create = new Button( this);
        create.setText("create profile");
        create.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                for (int i = 0; i < MAX_COLORS; i++) {
                    if (findViewById(btn_ids.get(i)).getVisibility() == Button.VISIBLE) {
                        profile.colors[i] = colors[i];
                        profile.names[i] = texts[i].getText().toString();
                    }
                }
            }
        });


        LinearLayout.LayoutParams paramCreate = new LinearLayout.LayoutParams(70,70);
        create = new Button( this);
        create.setText("create profile");
        create.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < MAX_COLORS; i++) {
                    if (btns[i].getVisibility() == Button.VISIBLE) {
                        profile.colors[i] = colors[i];
                        profile.names[i] = texts[i].getText().toString();
                    }

                }
                try {
                    FileInputStream fis = v.getContext().openFileInput(SAVED_PROFILES_NAME);
                    ObjectInputStream is = new ObjectInputStream(fis);
                    container = (Auction_Profile_Container) is.readObject();
                    is.close();
                    fis.close();
                }
                catch(Exception e) {
                    return;
                }
                container.get_list().add(profile);
                try {
                    FileOutputStream fos = v.getContext().openFileOutput(SAVED_PROFILES_NAME, Context.MODE_PRIVATE);
                    ObjectOutputStream os = new ObjectOutputStream(fos);
                    os.writeObject(container);
                    os.close();
                    fos.close();
                }

                catch(Exception e) {
                    return;
                }
            }
        });
        rel.addView(create,paramCreate);

    }
}
