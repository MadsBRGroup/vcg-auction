package com.example.vcg_auction.ProfileActivity;

import java.io.Serializable;
import java.util.Arrays;
public class Auction_Profile implements Serializable {
    public final int MAX_PLAYER_NR;
    public final int MAX_COLOR_NR;
    public int def_player;
    public int def_color;
    public String profile_name;
    public String[] names;
    public String[] colors;
    public String[] color_names;
    public Auction_Profile(String name,int max_player_nr,int max_col_nr,int def_player,int def_color){
        names = new String[max_col_nr];
        colors = new String[max_col_nr];
        profile_name = name;
        MAX_COLOR_NR = max_col_nr;
        MAX_PLAYER_NR = max_player_nr;
        this.def_player = def_player;
        this.def_color = def_color;
    }
}
