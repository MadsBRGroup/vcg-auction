package com.example.vcg_auction.ProfileActivity;

import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class OnClickListenerToggleItem implements View.OnClickListener {
    View[] views;

    public OnClickListenerToggleItem(View[] views){
        this.views=views;
    }

    @Override
    public void onClick(View v) {
        ViewGroup vg = (ViewGroup) v.getParent();
        for( View vs : views){
            vg.removeView(vs);
        }
        vg.removeView(v);

    }
}
