package com.example.vcg_auction.ProfileActivity;

import android.content.DialogInterface;
import android.graphics.Color;
import android.view.View;
import android.widget.Button;

import com.example.vcg_auction.Auxillary.MyDrawableCompat;
import com.skydoves.colorpickerview.ColorEnvelope;
import com.skydoves.colorpickerview.ColorPickerDialog;
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener;

public class OnClickListenerChangeColor implements View.OnClickListener {
    String color_hex = "";
    int btn_nr;
    CreateItems activity;

    public OnClickListenerChangeColor(CreateItems activity,int btn_nr){
        this.btn_nr = btn_nr;
        this.activity = activity;
    }

    @Override
    public void onClick(View v) {
        new ColorPickerDialog.Builder(v.getContext())
                .setTitle("ColorPicker Dialog")
                .setPreferenceName("MyColorPickerDialog")
                .setPositiveButton("confirm",
                        new ColorEnvelopeListener() {
                            @Override
                            public void onColorSelected(ColorEnvelope envelope, boolean fromUser) {
                                color_hex = "#" + envelope.getHexCode().substring(2);
                                System.out.println(btn_nr);
                                System.out.println(activity.btns.length);
                                Button btn = activity.btns[btn_nr];
                                MyDrawableCompat.setColorFilter(activity.btns[btn_nr].getBackground(), Color.parseColor(color_hex));
                            }
                        })
                .setNegativeButton("cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                .attachAlphaSlideBar(false) // the default value is true.
                .attachBrightnessSlideBar(true)  // the default value is true.
                .setBottomSpace(12).show(); // set a bottom space between the last slidebar and buttons.
    }

    public String get_color(){
        return color_hex;
    }
}


