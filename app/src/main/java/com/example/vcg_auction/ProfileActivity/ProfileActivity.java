package com.example.vcg_auction.ProfileActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.vcg_auction.R;

public class ProfileActivity extends AppCompatActivity {


    Button begin, profile_btn;
    final static int MAX_PLAYER = 5;
    final static int MAX_COLORS = 10;
    Auction_Profile profile;
    String color_hex;
    EditText textitem, textplayer_nr,textprofilename;
    boolean filled_correctly;
    public Button create;
    String[] colors = {"#FFA500", "#D3212D", "#0048BA", "#BEBEBE", "#676767", "#644117", "#FFE135", "#008000","#9370DB"};
    String[] color_names = {"orange"  ,  "red"    , "blue"    , "white", "grey"   , "brown"   , "yellow", "green", "purple"};
    String[] names = {"orange"  ,  "red"    , "blue"    , "white", "grey"   , "brown"   , "yellow", "green", "purple"};



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        textitem = (EditText) findViewById(R.id.enteritemNumber);
        textprofilename = (EditText) findViewById(R.id.enterProfileName);
        textplayer_nr =(EditText) findViewById(R.id.enterPlayerNumber);
        int height = 25;
        int width = 25;
        int current_id;
        create = findViewById(R.id.btn_continue);
        create.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View v) {
                                          filled_correctly = true;
                                          String name_holder = textprofilename.getText().toString();
                                          if(name_holder.trim().isEmpty()){
                                              filled_correctly = false;
                                          }
                                          profile = new Auction_Profile(name_holder, get_input_with_default(textplayer_nr,MAX_PLAYER), get_input_with_default(textitem,MAX_COLORS),Math.min(MAX_PLAYER,4),MAX_COLORS);
                                          if(filled_correctly) {
                                              Intent intent = new Intent(v.getContext(), CreateItems.class);
                                              intent.putExtra("profile", profile);
                                              startActivity(intent);
                                          }
                                      }
                                  }
        );
    }

    public int get_input_with_default(EditText view,int def){
        String str = view.getText().toString().trim();
        if( str.isEmpty() ){
            return def;
        }
        else{
            return Integer.parseInt(str);
        }

    }
}

