package com.example.vcg_auction.ProfileActivity;

import java.util.ArrayList;
public class Auction_Profile_Container {
    ArrayList<Auction_Profile> profile_list = new ArrayList<Auction_Profile>();
    private int last_profile = 0;
    private int current_profile = 0;
    public Auction_Profile_Container(){
        this(false);
    }

    public Auction_Profile_Container(boolean set_empty){
        if( !set_empty) {
            Auction_Profile gaia_profile = new Auction_Profile("Gaia Project", 4, 7, 4, 7);
            gaia_profile.names[0] = "Geodens\nBal T'aks";
            gaia_profile.names[1] = "Ivits\nHadsch Hallas";
            gaia_profile.names[2] = "Terrans\nLantids";
            gaia_profile.names[3] = "Itars\nNevlas";
            gaia_profile.names[4] = "Firaks\nBescods";
            gaia_profile.names[5] = "Ambas\nTaklons";
            gaia_profile.names[6] = "Xenos\nGleens";

            gaia_profile.colors[0] = "#FFA500";
            gaia_profile.colors[1] = "#D3212D";
            gaia_profile.colors[2] = "#0048BA";
            gaia_profile.colors[3] = "#BEBEBE";
            gaia_profile.colors[4] = "#676767";
            gaia_profile.colors[5] = "#644117";
            gaia_profile.colors[6] = "#FFE135";
            add_profile(gaia_profile);

            Auction_Profile no_theme_profile = new Auction_Profile("Basic Setup", 5, 10, 4, 7);
            no_theme_profile.names[0] = "Orange";
            no_theme_profile.names[1] = "Red";
            no_theme_profile.names[2] = "Blue";
            no_theme_profile.names[3] = "White";
            no_theme_profile.names[4] = "Grey";
            no_theme_profile.names[5] = "Brown";
            no_theme_profile.names[6] = "Yellow";
            no_theme_profile.names[7] = "Green";
            no_theme_profile.names[8] = "Purple";
            no_theme_profile.names[9] = "Black";

            no_theme_profile.colors[0] = "#FFA500";
            no_theme_profile.colors[1] = "#D3212D";
            no_theme_profile.colors[2] = "#0048BA";
            no_theme_profile.colors[3] = "#BEBEBE";
            no_theme_profile.colors[4] = "#676767";
            no_theme_profile.colors[5] = "#644117";
            no_theme_profile.colors[6] = "#FFE135";
            no_theme_profile.colors[7] = "#008000";
            no_theme_profile.colors[8] = "#800080";
            no_theme_profile.colors[9] = "#000000";
            add_profile(no_theme_profile);

            Auction_Profile TM_profile = new Auction_Profile("Terra Mystica", 5, 9, 4, 9);
            TM_profile.names[0] = "Acolyte\nDragonlords";
            TM_profile.names[1] = "Chaos Mag.\nGiants";
            TM_profile.names[2] = "Swarmlings\nMermaids";
            TM_profile.names[3] = "Icemaidens\nYetis";
            TM_profile.names[4] = "Engineers\nDwarves";
            TM_profile.names[5] = "Cultists\nHalflings";
            TM_profile.names[6] = "Fakits\nNomads";
            TM_profile.names[7] = "Witches\nAuren";
            TM_profile.names[8] = "Darklings\nAlchemists";

            TM_profile.colors[0] = "#FFA500";
            TM_profile.colors[1] = "#D3212D";
            TM_profile.colors[2] = "#0048BA";
            TM_profile.colors[3] = "#BEBEBE";
            TM_profile.colors[4] = "#676767";
            TM_profile.colors[5] = "#644117";
            TM_profile.colors[6] = "#FFE135";
            TM_profile.colors[7] = "#008000";
            TM_profile.colors[8] = "#000000";
            add_profile(TM_profile);
        }

/*        Auction_Profile TeMa_profile = new Auction_Profile("Terraforming Mars", 5, 9, 4, 9);
        TeMa_profile.names[0] = "Acolyte\nDragonlords";
        TeMa_profile.names[1] = "Chaos Mag.\nGiants";
        TeMa_profile.names[2] = "Swarmlings\nMermaids";
        TeMa_profile.names[3] = "Icemaidens\nYetis";
        TeMa_profile.names[4] = "Engineers\nDwarves";
        TeMa_profile.names[5] = "Cultists\nHalflings";
        TeMa_profile.names[6] = "Fakits\nNomads";
        TeMa_profile.names[7] = "Witches\nAuren";
        TeMa_profile.names[8] = "Darklings\nAlchemists";

        TeMa_profile.colors[0] = "#FFA500";
        TeMa_profile.colors[1] = "#D3212D";
        TeMa_profile.colors[2] = "#0048BA";
        TeMa_profile.colors[3] = "#BEBEBE";
        TeMa_profile.colors[4] = "#676767";
        TeMa_profile.colors[5] = "#644117";
        TeMa_profile.colors[6] = "#FFE135";
        TeMa_profile.colors[7] = "#008000";
        TeMa_profile.colors[8] = "#000000";
        add_profile(TeMa_profile);
    }*/
    }

    public ArrayList<Auction_Profile> get_list(){
        return profile_list;
    }

    public void set_profile(int i){
        last_profile = current_profile;
        current_profile = i;
    }

    public void add_profile(Auction_Profile profile){
        get_list().add(profile);
    }

    public void add_profile(int i,Auction_Profile profile){
        get_list().add(i,profile);
    }

    public Auction_Profile get_profile(int i){return get_list().get(i);}

    public int getCurrent_profile(){
        return current_profile;
    }

    public void setLast_profile(int num){
    last_profile = num;
    }
}



