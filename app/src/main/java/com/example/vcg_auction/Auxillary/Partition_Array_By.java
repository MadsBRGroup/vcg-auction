package com.example.vcg_auction.Auxillary;
import java.util.ArrayList;
public abstract class Partition_Array_By<T> {
    public ArrayList<T> partition_true;
    public ArrayList<T> partition_false;
    public T[] arr;
    public Partition_Array_By(T[] arr){
        this.arr = arr;
        partition_true = new ArrayList<T>(arr.length);
        partition_false = new ArrayList<T>(arr.length);
    }
    public abstract boolean condition(int i);

    public void partition(){
        for(int i = 0; i < arr.length;i++){
            if(condition(i))
            {
                partition_true.add(arr[i]);
            }
            else{
                partition_false.add(arr[i]);
            }
        }
    }
}
