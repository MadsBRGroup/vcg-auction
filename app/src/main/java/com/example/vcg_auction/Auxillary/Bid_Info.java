package com.example.vcg_auction.Auxillary;

import com.example.vcg_auction.ProfileActivity.Auction_Profile;

public class Bid_Info implements java.io.Serializable {

    public String[] initials;
    public int player_nr;
    public Auction_Profile profile;
    public int[][] bids;
    public int color_nr;
    public boolean secret_bids;
    public final boolean TEST;
    public Bid_Info(String[] initials, int[][] bids,boolean secret_bids,Auction_Profile profile,int player_nr,int color_nr,boolean TEST){
        this.initials = initials;
        this.bids = bids;
        this.profile = profile;
        this.secret_bids = secret_bids;
        this.player_nr = player_nr;
        this.color_nr = color_nr;
        this.TEST = TEST;
    }
}
