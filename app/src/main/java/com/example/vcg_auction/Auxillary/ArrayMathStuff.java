package com.example.vcg_auction.Auxillary;
// Java program to implement 
// the next_permutation method 
  
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ArrayMathStuff {
    public int[] data;
    public int permutations_remaining = 0;
    public int permutations = 1;
    public boolean done = false;

    public ArrayMathStuff(int[] spec_data) {
        data = spec_data;
        for (int i = 1; i < data.length + 1; i++) {
            permutations *= i;
        }
        permutations_remaining = permutations;
        if (data.length > 1) {
            Boolean done = false;
        } else {
            Boolean done = true;
        }
    }

    // Function to swap the data 
    // present in the left and right indices 
    public int[] swap(int data[], int left, int right) {

        // Swap the data 
        int temp = data[left];
        data[left] = data[right];
        data[right] = temp;

        // Return the updated array 
        return data;
    }

    // Function to reverse the sub-array 
    // starting from left to the right 
    // both inclusive 
    public int[] reverse(int data[], int left, int right) {

        // Reverse the sub-array 
        while (left < right) {
            int temp = data[left];
            data[left++] = data[right];
            data[right--] = temp;
        }

        // Return the updated array 
        return data;
    }

    // Function to find the next permutation 
    // of the given integer array 
    public int[] findNextPermutation(int data[]) {
        if (permutations_remaining == 1) {
            done = true;
        }
        if (permutations_remaining == permutations) {
            permutations_remaining -= 1;
            return data;
        }

        // If the given dataset is empty 
        // or contains only one element 
        // next_permutation is not possible 
        if (data.length <= 1) {
            System.out.println("warning, array too small");
            return data;
        }

        int last = data.length - 2;

        // find the longest non-increasing suffix 
        // and find the pivot 
        while (last >= 0) {
            if (data[last] < data[last + 1]) {
                break;
            }
            last--;
        }

        // If there is no increasing pair 
        // there is no higher order permutation 
        if (last < 0) {
            System.out.println("warning, all permutations made, returning last permutation");
            return data;
        }
        int nextGreater = data.length - 1;

        // Find the rightmost successor to the pivot 
        for (int i = data.length - 1; i > last; i--) {
            if (data[i] > data[last]) {
                nextGreater = i;
                break;
            }
        }

        // Swap the successor and the pivot 
        data = swap(data, nextGreater, last);

        // Reverse the suffix 
        data = reverse(data, last + 1, data.length - 1);
        // Return true as the next_permutation is done 
        permutations_remaining -= 1;
        return data;
    }


    public int[] argsort(double[] main) {
        int[] index = new int[main.length];
        for (int i = 0; i < main.length; i++) {
            index[i] = i;
        }
        argsort(main, index, 0, main.length - 1);
        return index;
    }

    // argsort a[left] to a[right]
    public void argsort(double[] a, int[] index, int left, int right) {
        if (right <= left) return;
        int i = partition(a, index, left, right);
        argsort(a, index, left, i - 1);
        argsort(a, index, i + 1, right);
    }

    // partition a[left] to a[right], assumes left < right
    private int partition(double[] a, int[] index,
                          int left, int right) {
        int i = left - 1;
        int j = right;
        while (true) {
            while (less(a[++i], a[right]))      // find item on left to swap
                ;                               // a[right] acts as sentinel
            while (less(a[right], a[--j]))      // find item on right to swap
                if (j == left) break;           // don't go out-of-bounds
            if (i >= j) break;                  // check if pointers cross
            exch(a, index, i, j);               // swap two elements into place
        }
        exch(a, index, i, right);               // swap with partition element
        return i;
    }

    // is x < y ?
    private boolean less(double x, double y) {
        return (x < y);
    }

    // exchange a[i] and a[j]
    private void exch(double[] a, int[] index, int i, int j) {
        double swap = a[i];
        a[i] = a[j];
        a[j] = swap;
        int b = index[i];
        index[i] = index[j];
        index[j] = b;
    }

    public static void printMatrix(double[][] m) {
        for (int row = 0; row < m.length; row++)//Cycles through rows
        {
            for (int col = 0; col < m[row].length; col++)//Cycles through columns
            {
                System.out.print(m[row][col] + "  ");
            }
            System.out.println(); //Makes a new row
        }
    }

    public static void printMatrix(int[][] m) {
        for (int row = 0; row < m.length; row++)//Cycles through rows
        {
            for (int col = 0; col < m[row].length; col++)//Cycles through columns
            {
                System.out.print(m[row][col] + "  ");
            }
            System.out.println(); //Makes a new row
        }
    }

    public static int[][] toInt(Number[][] m ) {
        int[][] result = new int[m.length][m[0].length];
        for (int row = 0; row < m.length; row++)//Cycles through rows
        {
            for (int col = 0; col < m[row].length; col++)//Cycles through columns
            {
                result[row][col] =(int)m[row][col];
            }
        }
        return result;
    }
    public static int[][] toInt(double[][] m ) {
        int[][] result = new int[m.length][m[0].length];
        for (int row = 0; row < m.length; row++)//Cycles through rows
        {
            for (int col = 0; col < m[row].length; col++)//Cycles through columns
            {
                result[row][col] =(int)m[row][col];
            }
        }
        return result;
    }
    public static double[][] toDouble(int[][] m) {
        double[][] result = new double[m.length][m[0].length];
        for (int row = 0; row < m.length; row++)//Cycles through rows
        {
            for (int col = 0; col < m[row].length; col++)//Cycles through columns
            {
                result[row][col] =(double)m[row][col];
            }
        }
        return result;
    }


        public static boolean areDistinct(Integer arr[])
        {
            // Put all array elements in a HashSet
            Set<Integer> s = new HashSet<Integer>(Arrays.asList(arr));

            // If all elements are distinct, size of
            // HashSet should be same array.
            return (s.size() == arr.length);
        }

        public static boolean contains(int[] arr, int x) {
            for (int y : arr) {
                if(x==y){
                    return true;
                }
            }
            return false;
        }
        public static boolean contains(String[] arr, String x){
            Set<String> s = new HashSet<String>(Arrays.asList(arr));
            return (!s.add(x));
        }

        public static boolean is_identical(int[] arr, int x){
            for(int i = 0 ; i<arr.length;i++){
                if(arr[i] != x){
                    return false;
                }
            }
            return true;
        }
        public static boolean is_identical(int[] arr){return is_identical(arr,arr[0]);}

        public static int[][] transposed_copy(int[][] matrix){
            int i, j;
            int N=matrix.length;
            int M=matrix[0].length;
            int[][] result = new int[M][N];
            for (i = 0; i < M; i++)
                for (j = 0; j < N; j++)
                    result[i][j] = matrix[j][i];
            return result;
        }

        public static int[][] remove_rows(int[][] matrix,int[] rows) {
            int i,j;
            int new_i = 0;
            int[][] new_matrix = new int[matrix.length - rows.length][matrix[0].length];
            for (i = 0; i < matrix.length; i++) {
                if(!contains(rows,i)){
                    for(j = 0; j < matrix[0].length;j++) {
                        new_matrix[new_i][j] = matrix[i][j];
                    }
                    new_i+=1;
                }
            }
            return new_matrix;
        }

    public static int[][] remove_rows(int[][] matrix, List<Integer> rows) {
        return remove_rows(matrix,list_to_array(rows));
    }


    public static int[][] remove_cols(int[][] matrix,int[] cols){
            return transposed_copy(remove_rows(transposed_copy(matrix), cols));
        }

    public static int[][] remove_cols(int[][] matrix,List<Integer> cols){
        return transposed_copy(remove_rows(transposed_copy(matrix), cols));
    }
        public static int[][] insert_zero_rows(int[][] matrix,int[] rows){
            int[][] new_matrix = new int[matrix.length + rows.length][matrix[0].length];
            int i,j;
            int old_i = 0;
            for (i = 0; i < matrix.length; i++) {
                if(!contains(rows,i)){
                    for(j=0;j<matrix[0].length;j++)
                    new_matrix[i][j] = matrix[old_i][j];
                    old_i +=1;
                }
            }
            return new_matrix;
        }

    public static int[][] insert_zero_cols(int[][] matrix,int[] cols){
        return transposed_copy(insert_zero_rows(transposed_copy(matrix), cols));
    }

    public static int[] list_to_array(List<Integer> list){
            int[] arr = new int[list.size()];
            for(int i=0;i<list.size();i++){
                arr[i] = list.get(i);
            }
        return arr;
        }

    public static int[] arange(int lower_limit, int upper_limit){
        int[] result = new int[upper_limit-lower_limit];
        for(int i = 0;i < upper_limit-lower_limit;i++){
            result[i] = i + lower_limit;
        }
        return result;
    }

    public static int[] arange(int upper_limit){
        return arange(0,upper_limit);
    }

}
