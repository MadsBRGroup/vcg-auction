package com.example.vcg_auction.VCG;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.apache.commons.math3.util.Combinations;
public class SetCrossing {
    public boolean done = false;
    SetProduct cart;
    ArrayList<int[]> list_for_products,list_of_arrays;
    public int[] index_holder;
    int combination_size;
    Iterator<int[]> combinator;
    int[] current_combi;


    public SetCrossing(ArrayList<int[]> list_of_arrays, int combination_size) // Gives the union of all setProducts that can be made from taking (combination_size) arrays from start_combi
    {
        this.list_of_arrays=list_of_arrays;
        combinator = new Combinations(list_of_arrays.size(),combination_size).iterator();
        current_combi= new int[combination_size];
        index_holder = new int[combination_size];
        current_combi = combinator.next();
        reset_product_list();
         
    }

    public int[] next_combi() {
        if (cart.done) {
            if (combinator.hasNext()) {
                current_combi = combinator.next();
                reset_product_list();
            }
        }
//        System.out.println("ARE WE DONE: " + Boolean.toString(cart.done & (!combinator.hasNext())));

        index_holder = cart.next_combi();
        if (cart.done & (!combinator.hasNext())) {
            done = true;
        }
//        System.out.println("ITERATING TO " + Arrays.toString(index_holder));
        return index_holder;
    }

    public void reset_product_list(){
        list_for_products= new ArrayList<int[]>();
        for(int x : current_combi){
            list_for_products.add(list_of_arrays.get(x));
        }
        cart = new SetProduct(list_for_products);
    }
}
