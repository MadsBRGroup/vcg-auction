package com.example.vcg_auction.VCG;
import java.util.ArrayList;


public class SetProduct{
	public ArrayList<int[]> combi_list = new ArrayList<int[]>();
	public int[] current_indices;
	public boolean done = false;
	public int[] lengths;
	public int size;
	int total;
	int remaining;


	public SetProduct(ArrayList<int[]> start_combi)
	{
		combi_list = start_combi;
		size = combi_list.size();
		lengths = new int[size];
		total = 1;
		for(int i = 0;i<size;i++) {
			lengths[i] = combi_list.get(i).length;
			total*=lengths[i];
		}
		remaining = total;
		current_indices = new int[size];
		current_indices[0] = -1;
	}

	
	public int[] next_combi()
	{
		if (done) {
			return current_indices;
		}
		remaining -=1;
		current_indices[0] += 1;
		for(int i = 0;i<current_indices.length;i++) {
			if (current_indices[i] == lengths[i]) {
				current_indices[i] = 0;
				if (i < size-1) {
					current_indices[i+1] +=1;
				}
			}
			else {
				break;
			}
				

		}
// check if current indices = length1-1,length2-1,...,length3-1
		done = true;
		for(int i = 0;i<size;i++){
			if(current_indices[i] != lengths[i]-1) {
				done = false;
				break;
			}
		}

		int[] result= new int[current_indices.length];
		for(int i =0;i<current_indices.length;i++) {
			result[i] = combi_list.get(i)[current_indices[i]];
		}

	return result;
	}
	
}
