package com.example.vcg_auction.VCG;
import com.example.vcg_auction.Auxillary.ArrayMathStuff;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;

public class VCG_Auction extends VCG_Holder {
	private List<Integer> removed_players_lst;
	private List<Integer> kept_players_lst;
	private List<Integer> removed_items_lst;
	private List<Integer> kept_items_lst;
	private int removed_players_nr;
	private int removed_items_nr;
	private int[] shrink_winners;
	private int[][] t_bids;

	public VCG_Auction(int[][] pre_bids,boolean TEST)
	{
		super(pre_bids,TEST);
		t_bids = ArrayMathStuff.transposed_copy(bids);
	}

	public VCG_Auction(int[][] pre_bids) {
		this(pre_bids,false);
	}

	public void run_VCG() {
		// print
		Maximization maxi;
		int[][] shrinked_bids = shrink(bids); // This method also sets winners, dist, removed/kept/players/items.
		if( winners_found_nr > 0){
			maxi = new Maximization(shrinked_bids);
			set_winner_and_dist(maxi.maximization());
			distribute_to_losers();
			score = maxi.score;
			if(TEST){
				for (int i = 0; i<winners.length;i++){
					System.out.println("WINNER NUMBER " + Integer.toString(i) +  " IS " + winners[i] + "AND GETS " + dist[i] );
				}
			}
			if(winners_found_nr >1) {
				int[][] bid_wo_player = new int[shrinked_bids.length - 1][shrinked_bids[0].length];
				int[] row = new int[1];
				for (int i = 0; i < winners_found_nr; i++) {
					row[0] = shrink_winners[i];
					bid_wo_player = ArrayMathStuff.remove_rows(shrinked_bids,row);
					maxi = new Maximization(bid_wo_player);
					maxi.maximization();
					scores_wo_winner[i] = maxi.score;
				}
			}
			if(TEST){
				System.out.println("SCORE W/O player is");
				System.out.println(Arrays.toString(scores_wo_winner));
			}
			for (int i = 0; i < winners_found_nr; i++) {
				price[i] = bids[winners[i]][dist[i]] + scores_wo_winner[i] - score;
				if(TEST){
					System.out.println("PRICE FOR PLAYER "+Integer.toString(winners[i]) + " IS " + Double.toString(price[i]));
				}
			}
		}

		else{
			if(TEST){
				System.out.println("NO BIDS PLACED, MEANING THAT THIS VARIABLE SHOULD BE 0 AND IS " + Integer.toString(winners_found_nr));
			}
			distribute_to_losers();
			winners_found_nr = 0;
		}
	}

	public void run_VCG(int[][] bids) {
		double[][] new_bids = ArrayMathStuff.toDouble(bids);
		run_VCG();
	}

	public VCG_Holder constructHolder(int[][] bids) {
		return new VCG_Holder(bids);
	}


	public int[][] shrink(int[][] bids) {
		Partition_Array_By_Removed partition = new Partition_Array_By_Removed(bids);
		removed_players_lst = partition.partition_true;
		kept_players_lst = partition.partition_false;

		partition = new Partition_Array_By_Removed(t_bids);
		removed_items_lst = partition.partition_true;
		kept_items_lst = partition.partition_false;
		if(TEST){
			System.out.println("REMOVED PLAYERS " + removed_players_lst.toString());
			System.out.println("REMOVED ITEMS " + removed_items_lst.toString());

		}
		removed_players_nr = removed_players_lst.size();
		removed_items_nr = removed_items_lst.size();
		winners_found_nr = Math.min(player_nr- removed_players_nr, color_nr-removed_items_nr);
		winners = new int[winners_found_nr];
		dist = new int [winners_found_nr];
		if(removed_items_nr == color_nr | removed_players_nr ==  player_nr)
		{
			return null;
		}
		return ArrayMathStuff.remove_cols(ArrayMathStuff.remove_rows(bids,removed_players_lst), removed_items_lst);
	}
//
//	public int[][] expand_bids(int[][] bids) {
//		return ArrayMathStuff.insert_zero_cols((ArrayMathStuff.insert_zero_rows(bids, removed_players)), removed_items);
//	}

	public void set_winner_and_dist(int[][] shrinked_result) {
		shrink_winners = shrinked_result[0];
		int[] shrink_dist = shrinked_result[1];
		if(TEST){
			System.out.println("IN SHRINKED SYSTEM , THE WINNERS ARE " + Arrays.toString(shrink_winners));
			System.out.println("IN SHRINKED SYSTEM , THE WINNERS WON ITEMS " + Arrays.toString(shrink_dist));
		}
		for (int i = 0; i < shrink_winners.length; i++) {
			winners[i] = kept_players_lst.get(shrink_winners[i]);
			dist[i] = kept_items_lst.get(shrink_dist[i]);
		}
		if(TEST) {
			System.out.println("IN EXPANDED SYSTEM , THE WINNERS ARE " + Arrays.toString(winners));
			System.out.println("IN EXPANDED SYSTEM , THE WINNERS WON ITEMS " + Arrays.toString(dist));
		}
	}

//TODO: CHECK IF WE NEED TO FORCE EXTRAS IN MAIN ACTIVITY
//ADD EXTRA WINNERS

	public void distribute_to_losers(){
		if (col_play_min > winners_found_nr){
			int[] loser_players = new int[player_nr- winners_found_nr];
			int[] loser_items = new int[color_nr- winners_found_nr];
			int i;
			if( winners_found_nr > 0 ){
				int next_index = 0;
				int[] loser_items_indices = new int[0];  //artifical, redefined below
				for (i = 0; i < player_nr; i++) {
					if (!ArrayMathStuff.contains(winners, i)) {
						loser_players[next_index] = i;
						next_index += 1;
					}
				}
				next_index=0;
				for(i = 0;i<color_nr;i++){
					if( ! ArrayMathStuff.contains(dist,i)){
						loser_items[next_index] = i;
						next_index+=1;
					}
				}
			}
			else{
				for(i = 0;i<player_nr;i++){
					loser_players[i] = i;
				}
				for(i = 0;i<color_nr;i++){
					loser_items[i] = i;
				}
			}
			ArrayUtils.shuffle(loser_players);
			ArrayUtils.shuffle(loser_items);
			winners = ArrayUtils.addAll(winners,Arrays.copyOfRange(loser_players,0,col_play_min- winners_found_nr));
			dist = ArrayUtils.addAll(dist,Arrays.copyOfRange(loser_items,0,col_play_min- winners_found_nr));
			if(TEST) {
				System.out.println(Arrays.toString(winners));
				System.out.println(Arrays.toString(dist));
			}
		}
	}

}


