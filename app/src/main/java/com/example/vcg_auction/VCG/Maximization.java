package com.example.vcg_auction.VCG;

import com.example.vcg_auction.Auxillary.ArrayMathStuff;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;
public class Maximization {
	public double[][] bids;
	int player_nr;
	int color_nr;
	int col_play_min;
	public int score;


	public Maximization(int[][] start_bids) {
		bids = ArrayMathStuff.toDouble(start_bids);
		player_nr = bids.length;
		color_nr = bids[0].length;
		col_play_min = Math.min(player_nr,color_nr);
	}

	public Maximization(double[][] start_bids) {
		bids = start_bids;
		player_nr = bids.length;
		color_nr = bids[0].length;
		col_play_min = Math.min(player_nr,color_nr);
	}
	public int[][] maximization(){
		return maximization(false);
	}

	public int[][] maximization(boolean include_lower_ties) {
		double[][] bids_clone = new double[player_nr][color_nr];
		for (int i = 0; i < player_nr * color_nr; i++) {
			bids_clone[i / color_nr][i % color_nr] = bids[i / color_nr][i % color_nr];
		}
		score = 0;
		int equals_nr = 1;
		int[] indices = new int[player_nr];
		for (int i = 0; i < player_nr; i++) {
			indices[i] = i;
		}
		ArrayMathStuff perm = new ArrayMathStuff(indices);
		ArrayList<int[]> orders = new ArrayList<int[]>();
		ArrayList<int[]> best_cases = new ArrayList<int[]>();
		ArrayList<int[]> winners = new ArrayList<int[]>();

		for (int i = 0; i < player_nr; i++) {
			int[] order = perm.argsort(bids_clone[i]);
			double minimum = bids_clone[i][order[col_play_min - 1]];
			int lower_index = color_nr - col_play_min;
			if(include_lower_ties & color_nr>=player_nr) {
				for (int j = 1; j < color_nr - col_play_min + 1; j++) {
					//				System.out.println("is" + Double.toString(bids_clone[i][order[color_nr - player_nr - j]]) + " equal to " + Double.toString(minimum) + "  " + Boolean.toString(bids_clone[i][order[color_nr - player_nr - j]] == minimum));
					if (bids_clone[i][order[color_nr - col_play_min - j]] == minimum) {
						lower_index -= 1;
					}
				}
			}
			orders.add(Arrays.copyOfRange(order, lower_index, color_nr));
		}
		SetCrossing prod = new SetCrossing(orders,col_play_min);
		int sum;
		int[] item_combi;
		int[] player_combi;
		while (!prod.done) {
			 item_combi = prod.next_combi();
			if (distinct(item_combi)) {
				player_combi = prod.current_combi;
				sum = 0;
				for (int i = 0; i < col_play_min; i++) {
					sum += bids[player_combi[i]][item_combi[i]];

				}
				if (sum > score) {
					score = sum;
					best_cases.clear();
					winners.clear();
				}
				if (sum >= score) {
					best_cases.add(item_combi);
					winners.add(prod.current_combi);
				}
			}
		}

		Random rand = new Random();
		int i = rand.nextInt(best_cases.size());
		int[][] result = new int[2][col_play_min];
		result[0] = winners.get(i);
		result[1] =best_cases.get(i);
		return result;
	}

	public boolean distinct(int[] arr) {
		HashSet<Integer> tempSet = new HashSet<>();
		for (int x : arr) {
			if (!tempSet.add(x)) {
				return false;
			}
		}
		return true;
	}
}
