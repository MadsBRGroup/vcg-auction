package com.example.vcg_auction.VCG;

import java.util.List;

public class VCG_Holder{
    int player_nr;
    int color_nr;
    int col_play_min;
    int[][] bids;
    public double score;
    public double[] scores_wo_winner;
    public double[] price;
    public int[] winners;
    public int[] dist;
    public int[] removed_players_nr;
    public int winners_found_nr;
    public final boolean TEST;


    public VCG_Holder(int[][] pre_bids,boolean test){
        player_nr = pre_bids.length;
        color_nr = pre_bids[0].length;
        col_play_min = Math.min(player_nr,color_nr);
        bids =  pre_bids;
        scores_wo_winner = new double[col_play_min];
        price = new double[col_play_min];
        TEST = test;
    }
    public VCG_Holder(int[][] pre_bids){this(pre_bids, false);}
}
