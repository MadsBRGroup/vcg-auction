package com.example.vcg_auction.VCG;

import com.example.vcg_auction.Auxillary.ArrayMathStuff;
import com.example.vcg_auction.Auxillary.Partition_Array_By;
import org.apache.commons.lang3.ArrayUtils;

public class Partition_Array_By_Removed extends Partition_Array_By<Integer> {
    int[][] bids;

    Partition_Array_By_Removed(int[][] bids){
        super(ArrayUtils.toObject(ArrayMathStuff.arange(bids.length)));
        this.bids = bids;
        partition();
    }

    @Override
    public boolean condition(int index){
        return ArrayMathStuff.is_identical(bids[index], 0);
    }


}

