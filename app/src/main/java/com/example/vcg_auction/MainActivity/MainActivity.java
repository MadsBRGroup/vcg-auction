package com.example.vcg_auction.MainActivity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.TextView;
import com.example.vcg_auction.ProfileActivity.Auction_Profile;
import com.example.vcg_auction.ProfileActivity.Auction_Profile_Container;
import com.example.vcg_auction.BidActivity.BidActivity;
import com.example.vcg_auction.Auxillary.Bid_Info;
import com.example.vcg_auction.ProfileActivity.ProfileActivity;
import com.example.vcg_auction.R;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Iterator;
import org.apache.commons.math3.util.Combinations;

public class MainActivity extends AppCompatActivity {
    Combinations combi = new Combinations(5,2);
    Button begin,profile_btn;
    int player_nr;
    final boolean TEST = false;
    int color_nr;
    public Auction_Profile_Container container;
    Auction_Profile profile;
    public static final String SAVED_PROFILES_NAME = "profile.txt";
    Spinner spinner,spinnerTwo,spinnerProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set variables
        setContentView(R.layout.activity_main);
        Iterator<int[]> iter = combi.iterator();
        begin = (Button)findViewById(R.id.buttonBegin);
         profile_btn = (Button) findViewById(R.id.btnCreateProfile);
        container =read_container_if_exist();
        String [] profile_name_list = new String[container.get_list().size()];
        spinner = (Spinner) findViewById(R.id.spinner);
        spinnerTwo = (Spinner) findViewById(R.id.spinner2);
        set_default_profile_options(0);
        for(int i = 0; i < container.get_list().size();i++){
            profile_name_list[i] = container.get_profile(i).profile_name;
        }

        //spinners

        TextView optionsView = findViewById(R.id.textViewOptions);
        optionsView.setTypeface(optionsView.getTypeface(), Typeface.BOLD);
        spinnerProfile = findViewById(R.id.spinnerProfile);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item,profile_name_list);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinnerProfile.setAdapter(spinnerArrayAdapter);
        spinnerProfile.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                set_default_profile_options(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {

            }
        });

//button

        begin.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int profile_choice = spinnerProfile.getSelectedItemPosition();
                        boolean isChecked = ((CheckBox) findViewById(R.id.checkBox)).isChecked();
                        Bid_Info info = new Bid_Info(null,null,isChecked,container.get_profile(profile_choice),get_selection_as_int(spinner),get_selection_as_int(spinnerTwo),TEST);
                        Intent intent = new Intent(v.getContext(), BidActivity.class);
                        intent.putExtra("options_info",(Serializable) info);
                        startActivity(intent);
                        }
                    }
                );

        //TODO:Finish this and publish. Invisible button atm.
        profile_btn.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(v.getContext(), ProfileActivity.class);
                        startActivity(intent);
                    }
                }
                );

    }


    public int get_selection_as_int (Spinner spinner){
        return Integer.parseInt(spinner.getSelectedItem().toString());
    }
    public Auction_Profile_Container read_container_if_exist(){
        Auction_Profile_Container result = null;
        ObjectInputStream input;

            File container_file = new File(getFilesDir() + File.separator + SAVED_PROFILES_NAME);
            if (container_file.exists()) {
                try {
                    input = new ObjectInputStream(new FileInputStream(new File(new File(getFilesDir(), "") + File.separator + SAVED_PROFILES_NAME)));

                    result = (Auction_Profile_Container) input.readObject();
                    input.close();
                } catch (Exception e) {
                    System.out.println("SENDING ERROR NOW");
                    System.out.println(e.toString());
                    System.out.println("sad read");
                }
            }
            else {
                result = new Auction_Profile_Container();
                write_container(container);
            }



        return result;
    }

    public void write_container(Auction_Profile_Container container){
        ObjectOutput out = null;

        try {
            out = new ObjectOutputStream(new FileOutputStream(new File(getFilesDir(),"")+SAVED_PROFILES_NAME));
            out.writeObject(container);
            out.close();
        }
        catch(Exception e)
        {
            System.out.println("SadWrite");
        }
    }
    public void set_default_profile_options(int position){
        container.set_profile(position);
        profile = container.get_profile(position);
        color_nr = profile.MAX_COLOR_NR;
        player_nr = profile.MAX_PLAYER_NR;
        spinner.setSelection(player_nr-2);
        spinnerTwo.setSelection(color_nr-2);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item,str_range(2,player_nr+1));
        ArrayAdapter<String> spinnerTwoArrayAdapter = new ArrayAdapter<String>(this,   android.R.layout.simple_spinner_item,str_range(2,color_nr+1));
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTwoArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(spinnerArrayAdapter);
        spinnerTwo.setAdapter(spinnerTwoArrayAdapter);
        spinner.setSelection(profile.def_player-2);
        spinnerTwo.setSelection(profile.def_color-2);
    }
    public String[] str_range(int from,int to){
        String[] result = new String[to-from];
        for(int i=0;i<to-from;i++){
            result[i]=Integer.toString(from + i);
        }
        return result;
    }
}
